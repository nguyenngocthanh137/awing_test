import { useMemo } from "react";

import classNames from "classnames";

import { SubCampaigns } from "../type";

import check from "../assets/svg/check.svg";
import checkActive from "../assets/svg/check-active.svg";

type PropsType = {
	select: number;
	data: SubCampaigns;
	setSelect: (value: number) => void;
};

const SubCampain: React.FC<PropsType> = ({ data, select, setSelect }) => {
	const checkData = useMemo(() => {
		const total = data?.ads?.reduce((a, b) => a + Number(b.quantity), 0);

		const checkListAds = data?.ads?.filter(
			(x) => !x?.name || Number(x?.quantity) === 0 || !x?.quantity
		);

		let valid: boolean;

		if (data?.ads?.length === 0) {
			valid = false;
		} else {
			valid = checkListAds?.length > 0 || !data?.name ? false : true;
		}

		return { total, valid };
	}, [data]);

	return (
		<div
			className={classNames("campaign-item", {
				active: select === data?.id,
				error: !checkData?.valid,
			})}
			onClick={() => setSelect(data?.id)}
		>
			<div className="name">
				<p>{data?.name}</p>
				<img src={data?.status ? checkActive : check} alt="check" />
			</div>
			<h1>{checkData?.total}</h1>
		</div>
	);
};
export default SubCampain;
