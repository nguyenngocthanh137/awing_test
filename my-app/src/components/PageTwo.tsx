import { ChangeEvent, useEffect, useState } from "react";

import classNames from "classnames";

import Input from "./Input";
import SubCampain from "./SubCampain";

import { SubCampaigns, InputData } from "../type";

import trash from "../assets/svg/trash.svg";

type PropsType = {
	submit: boolean;
	onChange: (data: InputData) => void;
	handleAddData: () => void;
	setSelect: (value: number) => void;
	select: number;
	data: SubCampaigns[];
	handleAddAds: () => void;
	handleDeleteAds: (list: number[]) => void;
};

const PageTwo: React.FC<PropsType> = ({
	submit,
	onChange,
	handleAddData,
	data,
	setSelect,
	select,
	handleAddAds,
	handleDeleteAds,
}) => {
	const [listSelect, setListSelect] = useState<number[]>([]);

	useEffect(() => {
		setListSelect([]);
	}, [select]);

	const handleChangeNameCampaign = (newData: InputData) => {
		onChange({ ...newData, kind: "campaign" });
	};

	const handleChangeStatusCampaign = (e: ChangeEvent) => {
		const target = e.target as HTMLInputElement;
		onChange({ status: target?.checked, kind: "campaign" });
	};

	const handleChangeAds = (newData: InputData, index: number) => {
		onChange({ ...newData, kind: "adsCampaign", index });
	};

	const handleChangeListSelect = (e: ChangeEvent, index: number) => {
		const target = e.target as HTMLInputElement;

		let newList: number[];

		if (target?.checked) {
			newList = [...listSelect, index];
		} else {
			newList = listSelect?.filter((x) => x !== index);
		}

		setListSelect(newList);
	};

	const handleChangeAllListSelect = (e: ChangeEvent) => {
		const target = e.target as HTMLInputElement;
		let newList: number[];

		if (target?.checked) {
			newList = data?.[select]?.ads?.map((x, index) => index);
		} else {
			newList = [];
		}

		setListSelect(newList);
	};

	const handleDeleteAdsItem = (list: number[]) => {
		handleDeleteAds(list);
		setListSelect([]);
	};

	return (
		<div className="page-two">
			<div className="add-data">
				<div className="add-data-wrap">
					<button onClick={handleAddData}>+</button>
					{data?.map((x, index) => (
						<SubCampain
							key={index}
							data={x}
							select={select}
							setSelect={setSelect}
						/>
					))}
				</div>
			</div>
			<div className="name-status-wrap">
				<Input
					data={{
						value: data?.[select]?.name,
						placeholder: "Tên chiến dịch con *",
						submit: submit,
						required: true,
						name: "name",
					}}
					onChange={(newData: InputData) => handleChangeNameCampaign(newData)}
				/>
				<label className="status-wrap">
					<input
						type="checkbox"
						checked={data?.[select]?.status}
						onChange={(e: ChangeEvent) => handleChangeStatusCampaign(e)}
					/>
					<p>Đang hoạt động</p>
				</label>
			</div>
			<div className="advertisement-wrap">
				<p className="title">DANH SÁCH QUẢNG CÁO</p>
				<table>
					<tbody>
						<tr>
							<th>
								<input
									type="checkbox"
									checked={
										listSelect?.length === data?.[select]?.ads?.length &&
										data?.[select]?.ads?.length > 0
									}
									onChange={(e: ChangeEvent) => handleChangeAllListSelect(e)}
								/>
							</th>
							<th>
								{listSelect?.length > 0 ? (
									<img
										src={trash}
										alt="trash"
										onClick={() => handleDeleteAdsItem(listSelect)}
									/>
								) : (
									<p>Tên quảng cáo *</p>
								)}
							</th>
							<th>{listSelect?.length === 0 && <p>Số lượng *</p>}</th>
							<th>
								<button onClick={handleAddAds}>+ THÊM</button>
							</th>
						</tr>
						{data?.[select]?.ads?.map((x, index) => (
							<tr
								key={index}
								className={classNames({
									active: listSelect?.includes(index),
									disableTrash: listSelect?.length > 0,
								})}
							>
								<td>
									<input
										type="checkbox"
										checked={listSelect?.includes(index)}
										onChange={(e: ChangeEvent) =>
											handleChangeListSelect(e, index)
										}
									/>
								</td>
								<td>
									<Input
										data={{
											value: x?.name,
											submit: submit,
											placeholder: "",
											required: true,
											name: "name",
										}}
										onChange={(newData: InputData) =>
											handleChangeAds(newData, index)
										}
									/>
								</td>
								<td>
									<Input
										data={{
											value: x?.quantity,
											submit: submit,
											placeholder: "",
											required: true,
											name: "quantity",
											type: "number",
										}}
										onChange={(newData: InputData) =>
											handleChangeAds(newData, index)
										}
									/>
								</td>
								<td>
									<img
										src={trash}
										alt="trash"
										onClick={() => handleDeleteAdsItem([index])}
									/>
								</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		</div>
	);
};

export default PageTwo;
