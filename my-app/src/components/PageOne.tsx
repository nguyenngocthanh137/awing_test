import { Information, InputData } from "../type";

import Input from "./Input";

type PropsType = {
	data: Information;
	submit: boolean;
	onChange: (data: InputData) => void;
};

const PageOne: React.FC<PropsType> = ({ data, submit, onChange }) => {
	const handleOnChange = (inputData: InputData) => {
		onChange({ ...inputData, kind: "information" });
	};

	return (
		<div className="page-one">
			<Input
				data={{
					value: data?.name,
					placeholder: "Tên chiến dịch *",
					required: true,
					submit,
					name: "name",
				}}
				onChange={handleOnChange}
			/>
			<Input
				data={{
					value: data?.describe,
					placeholder: "Mô tả",
					required: false,
					submit,
					name: "describe",
				}}
				onChange={handleOnChange}
			/>
		</div>
	);
};

export default PageOne;
