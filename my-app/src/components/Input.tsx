import { ChangeEvent } from "react";

import classNames from "classnames";

import { InputData } from "../type";

type PropsType = {
	data: {
		value: string | number;
		placeholder: string;
		submit: boolean;
		required: boolean;
		name: string;
		type?: string;
	};

	onChange?: (data: InputData) => void;
};

const Input: React.FC<PropsType> = ({ data, onChange }) => {
	const handleChangeInput = (e: ChangeEvent) => {
		const target = e.target as HTMLInputElement;

		const newData: InputData = {};

		newData[data?.name] = target.value;

		if (onChange) {
			onChange(newData);
		}
	};
	return (
		<div className={classNames("input-container")}>
			<div
				className={classNames("input-custom", {
					error: !data?.value && data?.submit && data?.required,
					valid: data?.value,
				})}
			>
				<input
					value={data?.value}
					onChange={handleChangeInput}
					type={data?.type || "text"}
				/>
				<span>{data?.placeholder}</span>
				<div className="border"></div>
			</div>
			<p className="error-text">Dữ liệu không hợp lệ</p>
		</div>
	);
};

export default Input;
