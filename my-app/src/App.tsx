import React, { useState } from "react";
import classNames from "classnames";

import { DataItem, InputData } from "./type";

import "./assets/css/main.css";

import PageOne from "./components/PageOne";
import PageTwo from "./components/PageTwo";

function App() {
	const [type, setType] = useState(1);
	const [campaign, setCampaign] = useState<DataItem>({
		information: {
			name: "",
			describe: "",
		},
		subCampaigns: [
			{
				id: 0,
				name: "Chiến dịch con 1",
				status: true,
				ads: [
					{
						name: "Quảng cáo 1",
						quantity: 0,
					},
				],
			},
		],
	});

	const [select, setSelect] = useState<number>(0);
	const [submit, setSubmit] = useState(false);

	const handleSubmit = () => {
		if (!campaign?.information?.name) {
			setTimeout(() => {
				window.alert("Vui lòng điền đúng và đầy đủ thông tin");
				setSubmit(true);
			}, 200);
		} else {
			const checkSubCampaign = campaign?.subCampaigns?.filter((x) => {
				const checkAds = x?.ads?.filter(
					(y) => !y?.name || Number(y?.quantity) === 0 || !y?.quantity
				);
				return !x?.name || checkAds?.length > 0 || x?.ads?.length === 0;
			});

			if (checkSubCampaign?.length > 0) {
				setTimeout(() => {
					window.alert("Vui lòng điền đúng và đầy đủ thông tin");
					setSubmit(true);
				}, 200);
			} else {
				window.confirm("Thành công");
				setSubmit(false);
			}
		}
	};

	const handleChange = (data: InputData) => {
		let newData;
		if (data?.kind === "information") {
			newData = {
				...campaign,
				information: { ...campaign.information, ...data },
			};
		} else if (data?.kind === "campaign") {
			newData = {
				...campaign,
				subCampaigns: campaign?.subCampaigns?.map((x, index) => {
					if (index === select) {
						return { ...x, ...data };
					} else {
						return x;
					}
				}),
			};
			setCampaign(newData);
		} else if (data?.kind === "adsCampaign") {
			newData = {
				...campaign,
				subCampaigns: campaign?.subCampaigns?.map((x) => {
					if (x?.id === select) {
						return {
							...x,
							ads: x?.ads?.map((y, index) => {
								if (index === data?.index) {
									return { ...y, ...data };
								} else {
									return y;
								}
							}),
						};
					} else {
						return x;
					}
				}),
			};
		}

		if (newData) {
			setCampaign(newData);
		}
	};

	const handleAddData = () => {
		setCampaign({
			...campaign,
			subCampaigns: [
				...campaign?.subCampaigns,
				{
					id: campaign?.subCampaigns.length,
					name: `Chiến dịch con ${campaign?.subCampaigns.length + 1}`,
					status: true,
					ads: [
						{
							name: `Quảng cáo 1`,
							quantity: 0,
						},
					],
				},
			],
		});

		setSelect(campaign?.subCampaigns.length);
	};

	const handleDeleteAds = (list: number[]) => {
		const newData = {
			...campaign,
			subCampaigns: campaign?.subCampaigns?.map((x) => {
				if (x?.id === select) {
					return {
						...x,
						ads: x?.ads?.filter((x, index) => !list?.includes(index)),
					};
				} else {
					return x;
				}
			}),
		};
		setCampaign(newData);
	};

	const handleAddAds = () => {
		const newData = {
			...campaign,
			subCampaigns: campaign?.subCampaigns?.map((x) => {
				if (x?.id === select) {
					return {
						...x,
						ads: [
							...x?.ads,
							{ name: `Quảng cáo ${x?.ads?.length + 1}`, quantity: 0 },
						],
					};
				} else {
					return x;
				}
			}),
		};
		setCampaign(newData);
	};

	return (
		<div className="test">
			<div className="header">
				<button onClick={handleSubmit}>SUBMIT</button>
			</div>
			<div className="body">
				<div className="wrap">
					<div
						className={classNames("wrap-header", {
							left: type === 1,
							right: type === 2,
						})}
					>
						<button onClick={() => setType(1)}>THÔNG TIN</button>
						<button onClick={() => setType(2)}>CHIẾN DỊCH CON</button>
					</div>
					<div className="wrap-body">
						{type === 1 ? (
							<PageOne
								data={campaign.information}
								submit={submit}
								onChange={handleChange}
							/>
						) : (
							<PageTwo
								submit={submit}
								onChange={handleChange}
								handleAddData={handleAddData}
								data={campaign?.subCampaigns}
								setSelect={setSelect}
								select={select}
								handleAddAds={handleAddAds}
								handleDeleteAds={handleDeleteAds}
							/>
						)}
					</div>
				</div>
			</div>
		</div>
	);
}

export default App;
