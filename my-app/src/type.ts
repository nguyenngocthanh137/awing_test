export interface Information {
    name: string;
    describe: string;
}

export interface Ads {
    name: string;
    quantity: number;
}

export interface SubCampaigns {
    id: number,
    name: string;
    status: boolean;
    ads: Ads[];
}

export interface DataItem {
    information: Information,
    subCampaigns: SubCampaigns[];
};

export interface InputData {
    [key: string]: string | boolean | number;
};